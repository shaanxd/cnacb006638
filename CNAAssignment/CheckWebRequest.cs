﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CNAAssignment
{
    class CheckWebRequest
    {
        public static bool checkValidity(String url)
        {
            try
            {
                //WebRequest is created for the given URL
                var webRequest = WebRequest.Create(url); //request is created to the specified URL
                webRequest.Timeout = 5000;
                webRequest.Method = "HEAD";

                using (var response = (HttpWebResponse)webRequest.GetResponse())
                {
                    response.Close();
                    return response.StatusCode == HttpStatusCode.OK;
                    //HttpStatusCode.OK means the URL is valid.
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static String getFreeSpace(String location)
        {
            DriveInfo drive = new DriveInfo(location);
            //Get DriveInfo of the given location
            double x = (Math.Round((drive.TotalFreeSpace / 1024d / 1024d / 1024d),2));
            //Get DriveInfo objects TotalFreeSpace
            return x.ToString()+" GBs Available";
        }
    }
}
