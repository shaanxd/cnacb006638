﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CNAAssignment
{
    class CustomWebClient:WebClient
    {
        private readonly long from; //existing file size

        public CustomWebClient(long from)
        {
            this.from = from;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.AddRange(this.from); //the existing file size is added to the request to start the download from the existing byte
            return request; //requeust object is returned
        }
    }
}
