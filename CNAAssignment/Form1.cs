﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNAAssignment
{
    public partial class mainFrm : Form
    {
        private int maxNumberOfDownloads = 5; //default maximum downloads
        private int currentDownloads = 0; //on going downloads
        private String fileLocation = "C:\\Downloads"; //defaullt download location
        public mainFrm()
        {
            InitializeComponent();
            System.Net.ServicePointManager.DefaultConnectionLimit = 100; //Setting the default connection limit
            if (!Directory.Exists(fileLocation))
            {
                Directory.CreateDirectory(fileLocation); //file location is created if it doesnt exist
            }
        }

        private void downloadBtn_Click(object sender, EventArgs e)
        {
            downloadFrm dwnld = new downloadFrm(this); // download form is created and viewed
            dwnld.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public String getFileLocation()
        {
            return fileLocation;
        }
        public void setFileLocation(String fileLocation)
        {
            this.fileLocation = fileLocation;
        }
        public int getMaxNumberOfDownloads()
        {
            return maxNumberOfDownloads;
        }
        public void setMaxNumberOfDownloads( int maxNumberOfDownloads)
        {
            this.maxNumberOfDownloads = maxNumberOfDownloads;
        }
        public int getCurrentDownloads()
        {
            return currentDownloads;
        }
        public void setCurrentDownloads(int currentDownloads)
        {
            this.currentDownloads = currentDownloads;
        }

        private void settingsBtn_Click(object sender, EventArgs e)
        {
            settingFrm settingFrm = new settingFrm(this); //setting form is created and viewed
            settingFrm.ShowDialog();
        }

        private void openLocationBtn_Click(object sender, EventArgs e)
        {
            Process.Start(fileLocation);
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            foreach (downloadControl download in downloadsPanel.Controls.OfType<downloadControl>().ToArray()) //check each download in downloads panel
            {
                if ((download.getDownloadState().Equals("Completed")) ||
                    (download.getDownloadState().Equals("Failed")) ||
                    (download.getDownloadState().Equals("Stopped"))) //download is disposed and remove if status is completed, failed or stopped
                {
                    download.Dispose();
                    downloadsPanel.Controls.Remove(download);
                }
            }
        }

        private void minimizeBtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void multipleBtn_Click(object sender, EventArgs e)
        {
            multiFrm frm = new multiFrm(this); //multifrm form is created and displayed
            frm.ShowDialog();
        }
    }
}
