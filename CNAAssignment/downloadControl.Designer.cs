﻿namespace CNAAssignment
{
    partial class downloadControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(downloadControl));
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.fileSize = new System.Windows.Forms.Label();
            this.downloadProgressBar = new System.Windows.Forms.ProgressBar();
            this.downloadStatus = new System.Windows.Forms.Label();
            this.downloadSpeed = new System.Windows.Forms.Label();
            this.estimatedTime = new System.Windows.Forms.Label();
            this.stopBtn = new System.Windows.Forms.Button();
            this.startBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.totalSize = new System.Windows.Forms.Label();
            this.pauseBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileNameLabel.Location = new System.Drawing.Point(3, 3);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(222, 25);
            this.fileNameLabel.TabIndex = 0;
            this.fileNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fileSize
            // 
            this.fileSize.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileSize.Location = new System.Drawing.Point(231, 3);
            this.fileSize.Name = "fileSize";
            this.fileSize.Size = new System.Drawing.Size(80, 25);
            this.fileSize.TabIndex = 1;
            this.fileSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // downloadProgressBar
            // 
            this.downloadProgressBar.Location = new System.Drawing.Point(403, 3);
            this.downloadProgressBar.Name = "downloadProgressBar";
            this.downloadProgressBar.Size = new System.Drawing.Size(149, 25);
            this.downloadProgressBar.TabIndex = 2;
            // 
            // downloadStatus
            // 
            this.downloadStatus.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadStatus.Location = new System.Drawing.Point(558, 3);
            this.downloadStatus.Name = "downloadStatus";
            this.downloadStatus.Size = new System.Drawing.Size(113, 25);
            this.downloadStatus.TabIndex = 3;
            this.downloadStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // downloadSpeed
            // 
            this.downloadSpeed.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadSpeed.Location = new System.Drawing.Point(678, 3);
            this.downloadSpeed.Name = "downloadSpeed";
            this.downloadSpeed.Size = new System.Drawing.Size(117, 25);
            this.downloadSpeed.TabIndex = 4;
            this.downloadSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // estimatedTime
            // 
            this.estimatedTime.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estimatedTime.Location = new System.Drawing.Point(801, 3);
            this.estimatedTime.Name = "estimatedTime";
            this.estimatedTime.Size = new System.Drawing.Size(96, 25);
            this.estimatedTime.TabIndex = 5;
            this.estimatedTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // stopBtn
            // 
            this.stopBtn.Enabled = false;
            this.stopBtn.FlatAppearance.BorderSize = 0;
            this.stopBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stopBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopBtn.Image = ((System.Drawing.Image)(resources.GetObject("stopBtn.Image")));
            this.stopBtn.Location = new System.Drawing.Point(1051, 3);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(68, 25);
            this.stopBtn.TabIndex = 6;
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // startBtn
            // 
            this.startBtn.FlatAppearance.BorderSize = 0;
            this.startBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startBtn.Image = ((System.Drawing.Image)(resources.GetObject("startBtn.Image")));
            this.startBtn.Location = new System.Drawing.Point(903, 3);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(68, 25);
            this.startBtn.TabIndex = 7;
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1125, 2);
            this.label1.TabIndex = 8;
            // 
            // totalSize
            // 
            this.totalSize.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalSize.Location = new System.Drawing.Point(317, 3);
            this.totalSize.Name = "totalSize";
            this.totalSize.Size = new System.Drawing.Size(80, 25);
            this.totalSize.TabIndex = 9;
            this.totalSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pauseBtn
            // 
            this.pauseBtn.Enabled = false;
            this.pauseBtn.FlatAppearance.BorderSize = 0;
            this.pauseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pauseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseBtn.Image = ((System.Drawing.Image)(resources.GetObject("pauseBtn.Image")));
            this.pauseBtn.Location = new System.Drawing.Point(977, 3);
            this.pauseBtn.Name = "pauseBtn";
            this.pauseBtn.Size = new System.Drawing.Size(68, 25);
            this.pauseBtn.TabIndex = 10;
            this.pauseBtn.UseVisualStyleBackColor = true;
            this.pauseBtn.Click += new System.EventHandler(this.pauseBtn_Click);
            // 
            // downloadControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.pauseBtn);
            this.Controls.Add(this.totalSize);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.stopBtn);
            this.Controls.Add(this.estimatedTime);
            this.Controls.Add(this.downloadSpeed);
            this.Controls.Add(this.downloadStatus);
            this.Controls.Add(this.downloadProgressBar);
            this.Controls.Add(this.fileSize);
            this.Controls.Add(this.fileNameLabel);
            this.Name = "downloadControl";
            this.Size = new System.Drawing.Size(1132, 33);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.Label fileSize;
        private System.Windows.Forms.ProgressBar downloadProgressBar;
        private System.Windows.Forms.Label downloadStatus;
        private System.Windows.Forms.Label downloadSpeed;
        private System.Windows.Forms.Label estimatedTime;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label totalSize;
        private System.Windows.Forms.Button pauseBtn;
    }
}
