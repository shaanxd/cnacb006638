﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace CNAAssignment
{
    public enum stateOfDownload
    {
        Resumed,
        Ready,
        Downloading,
        Completed,
        Stopped,
        Failed,
        Paused,
    }
    public partial class downloadControl : UserControl
    {
        mainFrm mainFrm;
        WebClient webClient;
        Stopwatch stopWatch;
        stateOfDownload downloadState;
        String fileName;
        String fileLocation;
        Uri downloadUrl;
        Thread downloadThread;
        Thread resumeThread;
        String fileExtension;
        String totalFilePath;
        String tempFilePath;
        long existFileSize;


        public downloadControl(mainFrm mainFrm, String downloadUrl, String fileLocation, String fileName)
        {
            InitializeComponent();
            this.mainFrm = mainFrm;
            this.downloadUrl = new Uri(downloadUrl);
            this.fileLocation = fileLocation;
            this.fileName = fileName;
            this.fileExtension = Path.GetExtension(downloadUrl);
            this.totalFilePath = fileLocation + "//" + fileName + fileExtension; //Full download location with location, name and extension.
            this.tempFilePath = fileLocation + "//" + fileName + ".tmp"; //Full download location for temp files with location, name and .tmp extension if the file were to be removed.
            webClient = new WebClient();
            stopWatch = new Stopwatch(); // Stopwatch object instantiated
            downloadThread = new Thread(beginDownload); //Thread is created
            fileNameLabel.Text = fileName + fileExtension;
            downloadState = stateOfDownload.Ready;
            updateDownloadStatus();

            if (mainFrm.getMaxNumberOfDownloads() > mainFrm.getCurrentDownloads())
            {
                downloadThread.Start(); //Thread is started if maximum download has not been reached
            }
        }


        private void beginDownload()
        {
            mainFrm.setCurrentDownloads(mainFrm.getCurrentDownloads() + 1); //Current downloads is incremented by 1 to denote that a download is ongoing
            stopBtnStatus(true);
            pauseBtnStatus(true);
            startBtnStatus(false);
            webClient.DownloadFileCompleted += webClient_DownloadFileCompleted; //Adding own event to DownloadFileCompleted event of WebClient class
            webClient.DownloadProgressChanged += webClient_DownloadProgressChanged; //Adding own event to DownloadProgressChanged event of WebClient class
            webClient.DownloadFileAsync(downloadUrl, totalFilePath); //Starting the download
            downloadState = stateOfDownload.Downloading;
            updateDownloadStatus();
            stopWatch.Start(); //Stopwatch timer is started
        }


        private void webClient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (downloadState == stateOfDownload.Paused) //Download is paused
            {
                if (File.Exists(tempFilePath))
                {
                    appendFiles(); //.tmp file is appended if the .tmp file exists
                }
                MessageBox.Show("Download has been paused.");
            }
            else if(downloadState == stateOfDownload.Stopped) //Download has stopped
            {
                if (File.Exists(tempFilePath))
                {
                    File.Delete(tempFilePath); //Temporary file is deleted
                }
                File.Delete(totalFilePath); // Original download file is deleted
                MessageBox.Show("Download has been stopped.");
                mainFrm.setCurrentDownloads(mainFrm.getCurrentDownloads() - 1); //Current downloads is decremented by 1 to denote that the download is completed
                startBtnStatus(false);
            }
            else
            {
                if (e.Error == null) //Download is completed
                {
                    if (File.Exists(tempFilePath))
                    {
                        appendFiles();
                    }
                    MessageBox.Show("Download Completed Successfully!");
                    downloadState = stateOfDownload.Completed;
                }
                else //Download has failed
                {
                    MessageBox.Show(e.Error.Message);
                    downloadState = stateOfDownload.Failed;
                    if (File.Exists(tempFilePath))
                    {
                        File.Delete(tempFilePath);
                    }
                    File.Delete(totalFilePath);
                }
                mainFrm.setCurrentDownloads(mainFrm.getCurrentDownloads() - 1);
                startBtnStatus(false);
            }
            updateDownloadStatus();
            pauseBtnStatus(false); //buttons are disabled to avoid critical exceptions
            stopBtnStatus(false);
            stopWatch.Stop();
            stopWatch.Reset();
            ((WebClient)sender).Dispose(); 
        }


        private void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            updateDownloadDetals(e);
            updateEstimatedTime(e);
            updateProgressBar(e);
        }
        private void updateDownloadDetals(DownloadProgressChangedEventArgs e)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    updateDownloadDetals(e);
                });
            else
            {
                if ((e.BytesReceived / 1024d / stopWatch.Elapsed.TotalSeconds) < 1024) //Checking whether to represent in Megabytes or KiloBytes
                {
                    downloadSpeed.Text = string.Format("{0} KBps", (e.BytesReceived / 1024d / stopWatch.Elapsed.TotalSeconds).ToString("0.00")); //equation to calculate download speed
                }
                else
                {
                    downloadSpeed.Text = string.Format("{0} MBps", (e.BytesReceived / 1024d / 1024d / stopWatch.Elapsed.TotalSeconds).ToString("0.00"));
                }
                if (((e.TotalBytesToReceive + existFileSize) / 1024d) < 1024)
                {
                    fileSize.Text = string.Format("{0} KB", ((e.BytesReceived + existFileSize) / 1024d).ToString("0.00")); //equation to get the downloaded file size
                    totalSize.Text = string.Format("{0} KB",((e.TotalBytesToReceive + existFileSize) / 1024d).ToString("0.00")); //equation to get the total file size
                }
                else
                {
                    fileSize.Text = string.Format("{0} MB", ((e.BytesReceived + existFileSize) / 1024d / 1024d).ToString("0.00"));
                    totalSize.Text = string.Format("{0} MB", ((e.TotalBytesToReceive + existFileSize) / 1024d / 1024d).ToString("0.00"));
                }
            }
        }


        private void updateEstimatedTime(DownloadProgressChangedEventArgs e)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    updateEstimatedTime(e);
                });
            else
            {
                estimatedTime.Text = TimeSpan.FromSeconds(Math.Round(((e.TotalBytesToReceive / 1024d) - (e.BytesReceived / 1024d)) / (e.BytesReceived / 1024d / stopWatch.Elapsed.TotalSeconds))).ToString(); //estimated time equation
            }
        }


        private void updateProgressBar(DownloadProgressChangedEventArgs e)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    updateProgressBar(e);
                });
            else
            {
                downloadProgressBar.Value = Convert.ToInt32((e.BytesReceived + existFileSize) * 100 / (e.TotalBytesToReceive + existFileSize)); //progress bar is updated
            }
        }


        private void updateDownloadStatus()
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    updateDownloadStatus();
                });
            else
            {
                downloadStatus.Text = downloadState.ToString(); //Download status is set to the downloadStatus label
            }
        }


        private void stopBtn_Click(object sender, EventArgs e)
        {
            downloadState = stateOfDownload.Stopped;
            webClient.CancelAsync(); //Download is cancelled
        }


        private void stopBtnStatus(bool status)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    stopBtnStatus(status);
                });
            else
            {
                stopBtn.Enabled = status;
            }
        }


        private void pauseBtnStatus(bool status)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    pauseBtnStatus(status);
                });
            else
            {
                pauseBtn.Enabled = status;
            }
        }


        private void startBtnStatus(bool status)
        {
            if (InvokeRequired)
                Invoke((MethodInvoker)delegate ()
                {
                    startBtnStatus(status);
                });
            else
            {
                startBtn.Enabled = status;
            }
        }


        private void startBtn_Click(object sender, EventArgs e)
        {
            if (downloadState == stateOfDownload.Ready) //If download hasnt started yet
            {
                if (mainFrm.getCurrentDownloads() < mainFrm.getMaxNumberOfDownloads())
                {
                    downloadThread.Start(); //start the downloading thread
                }
                else
                {
                    MessageBox.Show("Maximum number of downloads already in progress.");
                }
            }
            else if (downloadState == stateOfDownload.Paused) //if download has paused
            {
                resumeThread = new Thread(resumeDownload); //start the resuming thread
                resumeThread.Start();
            }
        }


        public String getDownloadState()
        {
            return downloadState.ToString();
        }


        private void pauseBtn_Click(object sender, EventArgs e)
        {
            downloadState = stateOfDownload.Paused;
            startBtnStatus(true);
            webClient.CancelAsync();
        }


        private void resumeDownload()
        {
            downloadState = stateOfDownload.Resumed;
            existFileSize = new FileInfo(totalFilePath).Length; //existing file length is taken using FileInfo
            webClient = new CustomWebClient(existFileSize); // existing file length is passed to CustomWebClient
            webClient.DownloadFileCompleted += webClient_DownloadFileCompleted;
            webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
            webClient.DownloadFileAsync(downloadUrl, tempFilePath); //start download from existing byte
            stopBtnStatus(true);
            startBtnStatus(false);
            pauseBtnStatus(true);
            stopWatch.Start();
            updateDownloadStatus();
        }


        private void appendFiles()
        {
            byte[] file = File.ReadAllBytes(tempFilePath); //.tmp file bytes is taken to byte array
            using (var stream = new FileStream(totalFilePath, FileMode.Append))
            {
                stream.Write(file, 0, file.Length); //byte arrays bytes are written to original file
            }
            File.Delete(tempFilePath); //.tmp file is deleted
        }
    }
}
