﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNAAssignment
{
    public partial class downloadFrm : Form
    {
        mainFrm mainFrm;
        public downloadFrm(mainFrm mainFrm)
        {
            InitializeComponent();
            this.mainFrm = mainFrm;
            txtLocation.Text = mainFrm.getFileLocation();
            freeSpace.Text = CheckWebRequest.getFreeSpace(txtLocation.Text); //get free space method is used to retrieve the available free space
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            downloadFile(); //download file method is called
        }

        private void txtUrl_TextChanged(object sender, EventArgs e)
        {
            if (!CheckWebRequest.checkValidity(txtUrl.Text)) //checking if the URl is valid
            {
                txtFileName.Text = null;
                startBtn.Enabled = false;
            }
            else
            {
                txtFileName.Text = Path.GetFileNameWithoutExtension(txtUrl.Text); //file name is retrieved from URL
                startBtn.Enabled = true;
            }
        }

        private void browseBtn_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog newDialog = new FolderBrowserDialog() { Description = "Choose path to save file." })
            {
                if (newDialog.ShowDialog() == DialogResult.OK)
                {
                    txtLocation.Text = newDialog.SelectedPath; //selected path is assigned to location label
                }
            }
        }

        private void txtLocation_TextChanged(object sender, EventArgs e)
        {
            freeSpace.Text = CheckWebRequest.getFreeSpace(txtLocation.Text);
        }
        private void downloadFile()
        {
            if (txtFileName.Text.Equals("")) //checking whether the filename is empty
            {
                MessageBox.Show("Please enter a File Name.");
            }
            else
            {
                if (File.Exists(txtLocation.Text+"//"+txtFileName.Text+Path.GetExtension(txtUrl.Text))) //checking whether the filename exists
                {
                    MessageBox.Show("File already exists!");
                }
                else
                {
                    downloadControl downloadBar = new downloadControl(mainFrm, txtUrl.Text, txtLocation.Text, txtFileName.Text); //creating the a downloadcontrol 
                    mainFrm.downloadsPanel.Controls.Add(downloadBar); //adding downloadcontrol object to panel
                    downloadBar.Dock = DockStyle.Top;
                    this.Close();
                }
            }
        }
    }
}
