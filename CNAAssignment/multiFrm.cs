﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNAAssignment
{
    public partial class multiFrm : Form
    {
        mainFrm frm;
        List<string> retrievedLinks = new List<string>(); //list to store retrieved links
        List<string> extensionList = new List<string>(); //list to store selected extensions
        public multiFrm(mainFrm frm)
        {
            InitializeComponent();
            this.frm = frm;
        }

        private void filterBtn_Click(object sender, EventArgs e)
        {
            if (pageUrl.Text.Equals("")) //checking whether URL is empty
            {
                MessageBox.Show("URL Cannot be empty.");
            }
            else
            {
                if (!CheckWebRequest.checkValidity(pageUrl.Text)) //checking whether URL is valid
                {
                    MessageBox.Show("URL is invalid");
                }
                else
                {
                    urlListBox.Items.Clear();
                    retrievedLinks.Clear();
                    WebBrowser webBrowser = new WebBrowser();
                    webBrowser.ScriptErrorsSuppressed = true; //ignoring javascript content in the loaded page
                    webBrowser.Url = new Uri(pageUrl.Text); //navigating the given URL
                    webBrowser.DocumentCompleted += webBrowser_DocumentCompleted;
                }
            }
        }

        void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            HtmlDocument src = ((WebBrowser)sender).Document;
            extractLink(src);
        }

        private void extractLink(HtmlDocument source)
        {
            HtmlElementCollection anchorList = source.GetElementsByTagName("a"); //anchor tags of the document are stored in the
            foreach (var item in anchorList)
            {
                urlListBox.Items.Add(((HtmlElement)item).GetAttribute("href")); //href attributes of the anchor tags are added to checkboxlist
            }
            foreach (var item in urlListBox.Items)
            {
                retrievedLinks.Add(item.ToString()); //the href attributes are also added to the retrieved links list
            }
        }

        private void checkList(String extension)
        {
            if (extensionList.Contains(extension))
            {
                extensionList.Remove(extension); //adding extension to the extension list
            }
            else
            {
                extensionList.Add(extension); //removing extension from the extension list
            }
            sortLinks();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".pdf");
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".zip");
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".doc");
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".exe");
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".jpg");
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".png");
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            checkList(".gif");
        }
        private void sortLinks()
        {
            List<string> temp = new List<string>();
            foreach (var item in retrievedLinks)
            {
                if (extensionList.Contains( //checking whether the extension list contains urls file extension
                         Path.GetExtension(item)) || extensionList.Count == 0)
                {
                    temp.Add(item); //adding url with extension to temporary list
                }
            }
            updateList(temp);
        }
        private void updateList(List<string> temp)
        {
            urlListBox.Items.Clear(); //checkbox is cleared
            foreach (var item in temp)
            {
                urlListBox.Items.Add(item); //filtered list is added to checkbox list
            }
        }

        private void addListBtn_Click(object sender, EventArgs e)
        {
            foreach(String downloadUrl in urlListBox.CheckedItems) //for each url is selected list
            {
                if (!CheckWebRequest.checkValidity(downloadUrl)) //checking if URL is valid
                {
                    MessageBox.Show(downloadUrl + " is an invalid URL");
                }
                else
                {
                    if (File.Exists(frm.getFileLocation()+"//"+Path.GetFileName(downloadUrl))) //checking if filename already exists
                    {
                        MessageBox.Show("File " + Path.GetFileName(downloadUrl) + " already exists!");
                    }
                    else
                    { //download is added
                        downloadControl downloadBar = new downloadControl(frm, downloadUrl, frm.getFileLocation(), Path.GetFileNameWithoutExtension(downloadUrl));
                        frm.downloadsPanel.Controls.Add(downloadBar);
                        downloadBar.Dock = DockStyle.Top;
                    }
                }
            }
            this.Close();
        }
    }
}
