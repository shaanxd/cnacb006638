﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CNAAssignment
{
    public partial class settingFrm : Form
    {
        mainFrm mainFrm;
        public settingFrm(mainFrm mainFrm)
        {
            InitializeComponent();
            this.mainFrm = mainFrm;
            txtLocation.Text = mainFrm.getFileLocation(); //default location is shown in the textbox
            maximumDownloads.Text = mainFrm.getMaxNumberOfDownloads().ToString(); //max downloads is shown in the textbox
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog newDialog = new FolderBrowserDialog() { Description = "Choose path to save file." })
            {
                if (newDialog.ShowDialog() == DialogResult.OK)
                {
                    txtLocation.Text = newDialog.SelectedPath;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(maximumDownloads.Text) > 0) { //checking if input number is above 0 to avoid assigning 0 as maximum number of downloads
                mainFrm.setFileLocation(txtLocation.Text);
                mainFrm.setMaxNumberOfDownloads(Int32.Parse(maximumDownloads.Text)); //max number of downloads is parsed and is set to mainforms max downloads variable
                this.Close();
            }
            else
            {
                MessageBox.Show("Max number of downloads cannot be set to 0");
            }
        }
    }
}
